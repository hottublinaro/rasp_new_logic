<?php class Machine_api extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Rest_api_model');
        $this->load->model('Process_model');
    }
    public function all_api()
    {
        $filtration_time = [];
        $data = [];
        $result = $this->Rest_api_model->get_filtration_time();
        for ($i = 1; $i <= 24; $i++) {
            $status = "ft_status_" . ($i);
            array_push($filtration_time, $result[0]->$status);
        }

        $besgo = $this->Rest_api_model->get_besgo();
        for ($i = 1; $i <= 4; $i++) {
            $backwash_status = "backwash_status_" . $i;
            if ($besgo[0]->$backwash_status == 1) {
                $backwash_state = "backwash_state_" . $i;
                $backwash_start = 'backwash_start_' . $i;
                $backwash_end = 'backwash_end_' . $i;
                $start_time = $this->Process_model->format_time($besgo[0]->$backwash_start);
                $end_time = $this->Process_model->format_time($besgo[0]->$backwash_end);
                $day = $this->Process_model->change_number_to_day($besgo[0]->$backwash_state);
                if ($day != '') {
                    array_push($data, $start_time . '-' . $end_time);
                }
            }
        }
        $arr = array(
            'setting_mode' => $this->Rest_api_model->get_setting_mode(),
            'filtration_time' => $filtration_time,
            'data_setting' => $this->Rest_api_model->get_data_setting(),
            'get_besgo' => $data,
            'besgo_setting' => $this->Rest_api_model->get_besgo_setting(),
            'substance' => $this->Rest_api_model->get_substance(),
            'ph' => $this->Rest_api_model->get_ph(),
            'orp' => $this->Rest_api_model->get_orp(),
            'apf' => $this->Rest_api_model->get_apf(),
            'chlorine' => $this->Rest_api_model->get_chlorine(),
            'machine_option' => $this->Rest_api_model->get_machine_option(),
            'heatpump' => $this->Rest_api_model->get_heatpump(),
            'night_time' => $this->Rest_api_model->get_setting_night_time(),
            'vmc_speed' => $this->Rest_api_model->get_vmc_speed(),
            'acos' => $this->Rest_api_model->get_aco()
        );
        echo json_encode($arr);
    }
    public function test()
    {
        $data = array();
        $besgo = $this->Rest_api_model->get_besgo();
        for ($i = 1; $i <= 4; $i++) {
            $backwash_status = "backwash_status_" . $i;
            if ($besgo[0]->$backwash_status == 1) {
                $backwash_state = "backwash_state_" . $i;
                $backwash_start = 'backwash_start_' . $i;
                $backwash_end = 'backwash_end_' . $i;
                $start_time = $this->Process_model->format_time($besgo[0]->$backwash_start);
                $end_time = $this->Process_model->format_time($besgo[0]->$backwash_end);
                $day = $this->Process_model->change_number_to_day($besgo[0]->$backwash_state);
                if ($day != '') {
                    array_push($data, $start_time . '-' . $end_time);
                }
            }
        }
        print_r($data);
    }
}
