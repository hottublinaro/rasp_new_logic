<?php class Mygrate_data extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
    }
    public function migrate_field()
    {

        if (!$this->db->field_exists('heater_3', 'machine_option')) {
            $field = array(
                'heater_3' => array(
                    'type' => 'INT',
                    'constraint' => 1,
                    'default' => 0,
                    'after' => 'heater_2'
                )
            );
            $this->dbforge->add_column('machine_option', $field);
        }
    }
}
