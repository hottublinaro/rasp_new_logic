<div class="container mt-5">
    <div class="row">
        <div class="col-md-12 mt-4">
            <div class="box-showing">
                <div class="tab-header text-center">
                    <h4>About</h4>
                </div>
                <div class="row">
                    <div class="col-md-8 mt-3">
                        <h3>Machine Code</h3>
                    </div>
                    <div class="col-md-4 mt-3">
                        <div class="float-end">
                            <?= $machine_code ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 mt-3">
                        <h5>Version 2024-01-08</h5>
                    </div>
                    <div class="col-md-12">
                        <p>- อัพเดทระบบ การอัพเดท ย้ายไฟเขียนออกจาก โปรแกรม python <br />
                            - เพิ่มตั้งค่า night time จากระบบ <br />
                            - ปรับกระบวนการทำเงื่อนไข Filtration Auto เป็น Filter <br />
                            - ปรับกระบวนการทำเงื่อนไข Ph Orp เป็น Filter <br />
                            - ปรับกระบวนการเว็บเลือก Ph Orp ให้เลือกได้ฝั่งเดียว <br />
                            - ปรับกระบวนการทำ Backwash ให้เป็น วินาที ได้
                        </p>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
