<?php if ($status_heatpump[0]->vmc == 1) { ?>
    <div class="col-md-12 mt-4 ">
    <?php } else { ?>
        <div class="col-md-12 mt-4" style="display: none;">
        <?php } ?>
        <div class="box-showing">
            <div class="tab-header text-center">
                <h4>VMC Controller</h4>
            </div>
            <div class="row mt-3">
                <div class="col-md-12">
                    <div class="float-end">
                        <i class="fa fa-save fa-2x" onclick="submit_vmc()"></i>
                    </div>

                </div>
                <div class="col-md-12">
                    <div class="row mt-3">
                        <div class="col-md-8 mt-3">
                            <h3>Filtration Speed [ 0-100 ]</h3>
                        </div>
                        <div class="col-md-4 mt-3">
                            <div class="float-end">
                                <div class="input-group mb-3">
                                    <span class="input-group-text" id="basic-addon1" onclick="set_number_vmc('minus','vmc_filtration')"><i class="fa fa-minus fa-lg"></i></span>
                                    <input type="text" aria-label="Username" id="vmc_filtration" value="<?= ($vmc) ? $vmc[0]->vmc_filtration : 0 ?>" style="width: 90px; text-align : center">
                                    <span class="input-group-text" id="basic-addon2" onclick="set_number_vmc('plus','vmc_filtration')"><i class="fa fa-plus fa-lg"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-8 mt-3">
                            <h3>Ozone Choc speed [ 0-100 ]</h3>
                        </div>
                        <div class="col-md-4 mt-3">
                            <div class="float-end">
                                <div class="input-group mb-3">
                                    <span class="input-group-text" id="basic-addon1" onclick="set_number_vmc('minus','vmc_choc')"><i class="fa fa-minus fa-lg"></i></span>
                                    <input type="text" aria-label="Username" id="vmc_choc" value="<?= ($vmc) ? $vmc[0]->vmc_choc : 0 ?>" style="width: 90px; text-align : center">
                                    <span class="input-group-text" id="basic-addon2" onclick="set_number_vmc('plus','vmc_choc')"><i class="fa fa-plus fa-lg"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-8 mt-3">
                            <h3>Réglages Backwash speed [ 0-100 ]</h3>
                        </div>
                        <div class="col-md-4 mt-3">
                            <div class="float-end">
                                <div class="input-group mb-3">
                                    <span class="input-group-text" id="basic-addon1" onclick="set_number_vmc('minus','vmc_backwash')"><i class="fa fa-minus fa-lg"></i></span>
                                    <input type="text" aria-label="Username" id="vmc_backwash" value="<?= ($vmc) ? $vmc[0]->vmc_backwash : 0 ?>" style="width: 90px; text-align : center">
                                    <span class="input-group-text" id="basic-addon2" onclick="set_number_input('plus','vmc_backwash')"><i class="fa fa-plus fa-lg"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        </div>

        <script>
            function set_number_vmc(type, field) {
                if (type == 'plus') {
                    var number = $('#' + field).val();
                    if (parseInt(number) < 100) {
                        var sum = parseInt(number) + 1;
                        $('#' + field).val(sum)
                    }
                } else {
                    var number = $('#' + field).val();
                    if (parseInt(number) > 0) {
                        var sum = parseInt(number) - 1;
                        $('#' + field).val(sum)
                    }
                }
            }

            function submit_vmc() {
                if ($('#vmc_filtration').val() >= 0 && $('#vmc_filtration').val() <= 100) {
                    if ($('#vmc_choc').val() >= 0 && $('#vmc_choc').val() <= 100) {
                        if ($('#vmc_backwash').val() >= 0 && $('#vmc_backwash').val() <= 100) {
                            $.post("<?= base_url('admin/Advance/save_vmc') ?>", {
                                vmc_filtration: $('#vmc_filtration').val(),
                                vmc_choc: $('#vmc_choc').val(),
                                vmc_backwash: $('#vmc_backwash').val(),
                            }, function() {
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: 'Open online mode complete',
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                                setTimeout(function() {
                                    location.reload()
                                }, 2000);

                            })
                        } else {
                            error_number_vmc();
                        }
                    } else {
                        error_number_vmc();
                    }
                } else {
                    error_number_vmc();
                }
            }

            function error_number_vmc() {
                Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: 'Your information is incorrect.',
                    showConfirmButton: false,
                    timer: 1500
                })
            }
        </script>
