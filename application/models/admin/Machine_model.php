<?php class Machine_model extends CI_Model
{
    public function get_machine()
    {
        return $this->db->select('*')->from('machine')->get()->result();
    }
    public function update_machine($data)
    {
        $this->db->where('machine_id', 1)->update('machine', $data);
    }
    public function get_selection()
    {
        return $this->db->select('*')->from('machine_option')->get()->result();
    }
    public function update_selection($data)
    {
        $this->db->where('mo_id', 1)->update('machine_option', $data);
    }
    public function update_night_time_setting()
    {
        $this->db->where('night_time_id', 1)->update('night_time', array(
            'night_time_enable' => 0,
            'night_time_status' => 0
        ));
    }
    public function clear_backwash(){
        $this->db->where('backwash_id',1)->update('backwash',array(
            'backwash_state_1'=>'',
            'backwash_start_1'=>'00:00:00',
            'backwash_end_1'=>'00:00:00',
            'backwash_status_1'=>0,
            'backwash_state_2' => "",
            'backwash_start_2' => "00:00:00",
            'backwash_end_2' => "00:00:00",
            'backwash_status_2' => 0,
            'backwash_state_3' => "",
            'backwash_start_3' => "00:00:00",
            'backwash_end_3' => "00:00:00",
            'backwash_status_3' => 0,
            'backwash_state_4' => "",
            'backwash_start_4' => "00:00:00",
            'backwash_end_4' => "00:00:00",
            'backwash_status_4' => 0
        ));
    }
}
