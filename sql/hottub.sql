-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Mar 06, 2024 at 01:43 AM
-- Server version: 5.7.39
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hottub`
--

-- --------------------------------------------------------

--
-- Table structure for table `aco`
--

CREATE TABLE `aco` (
  `aco_id` int(1) NOT NULL,
  `aco_1` int(1) NOT NULL,
  `aco_2` int(1) NOT NULL,
  `aco_3` int(1) NOT NULL,
  `aco_4` int(1) NOT NULL,
  `aco_5` int(1) NOT NULL,
  `aco_6` int(1) NOT NULL,
  `aco_7` int(1) NOT NULL,
  `aco_8` int(1) NOT NULL,
  `aco_9` int(1) NOT NULL,
  `aco_10` int(1) NOT NULL,
  `aco_11` int(1) NOT NULL,
  `aco_12` int(1) NOT NULL,
  `aco_13` int(1) NOT NULL,
  `aco_14` int(1) NOT NULL,
  `aco_15` int(1) NOT NULL,
  `aco_16` int(1) NOT NULL,
  `aco_17` int(1) NOT NULL,
  `aco_18` int(1) NOT NULL,
  `aco_19` int(1) NOT NULL,
  `aco_20` int(1) NOT NULL,
  `aco_21` int(1) NOT NULL,
  `aco_22` int(1) NOT NULL,
  `aco_23` int(1) NOT NULL,
  `aco_24` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `aco`
--

INSERT INTO `aco` (`aco_id`, `aco_1`, `aco_2`, `aco_3`, `aco_4`, `aco_5`, `aco_6`, `aco_7`, `aco_8`, `aco_9`, `aco_10`, `aco_11`, `aco_12`, `aco_13`, `aco_14`, `aco_15`, `aco_16`, `aco_17`, `aco_18`, `aco_19`, `aco_20`, `aco_21`, `aco_22`, `aco_23`, `aco_24`) VALUES
(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `administrator`
--

CREATE TABLE `administrator` (
  `admin_id` int(11) NOT NULL,
  `admin_name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_userlogin` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_password` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `administrator`
--

INSERT INTO `administrator` (`admin_id`, `admin_name`, `admin_userlogin`, `admin_password`) VALUES
(1, 'administrator', 'administrator', '8cb2237d0679ca88db6464eac60da96345513964');

-- --------------------------------------------------------

--
-- Table structure for table `apf`
--

CREATE TABLE `apf` (
  `apf_id` int(1) NOT NULL,
  `apf_1` int(1) NOT NULL,
  `apf_2` int(1) NOT NULL,
  `apf_3` int(1) NOT NULL,
  `apf_4` int(1) NOT NULL,
  `apf_5` int(1) NOT NULL,
  `apf_6` int(1) NOT NULL,
  `apf_7` int(1) NOT NULL,
  `apf_8` int(1) NOT NULL,
  `apf_9` int(1) NOT NULL,
  `apf_10` int(1) NOT NULL,
  `apf_11` int(1) NOT NULL,
  `apf_12` int(1) NOT NULL,
  `apf_13` int(1) NOT NULL,
  `apf_14` int(1) NOT NULL,
  `apf_15` int(1) NOT NULL,
  `apf_16` int(1) NOT NULL,
  `apf_17` int(1) NOT NULL,
  `apf_18` int(1) NOT NULL,
  `apf_19` int(1) NOT NULL,
  `apf_20` int(1) NOT NULL,
  `apf_21` int(1) NOT NULL,
  `apf_22` int(1) NOT NULL,
  `apf_23` int(1) NOT NULL,
  `apf_24` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `apf`
--

INSERT INTO `apf` (`apf_id`, `apf_1`, `apf_2`, `apf_3`, `apf_4`, `apf_5`, `apf_6`, `apf_7`, `apf_8`, `apf_9`, `apf_10`, `apf_11`, `apf_12`, `apf_13`, `apf_14`, `apf_15`, `apf_16`, `apf_17`, `apf_18`, `apf_19`, `apf_20`, `apf_21`, `apf_22`, `apf_23`, `apf_24`) VALUES
(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `backwash`
--

CREATE TABLE `backwash` (
  `backwash_id` int(1) NOT NULL,
  `backwash_state_1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `backwash_start_1` time NOT NULL,
  `backwash_end_1` time NOT NULL,
  `backwash_status_1` int(1) NOT NULL COMMENT '0.close 1.open',
  `backwash_state_2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `backwash_start_2` time NOT NULL,
  `backwash_end_2` time NOT NULL,
  `backwash_status_2` int(1) NOT NULL COMMENT '0.close 1.open	',
  `backwash_state_3` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `backwash_start_3` time NOT NULL,
  `backwash_end_3` time NOT NULL,
  `backwash_status_3` int(1) NOT NULL COMMENT '0.close 1.open',
  `backwash_state_4` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `backwash_start_4` time NOT NULL,
  `backwash_end_4` time NOT NULL,
  `backwash_status_4` int(1) NOT NULL COMMENT '0.close 1.open',
  `backwash_mode` int(1) NOT NULL COMMENT '0.close 1.manual 2.auto',
  `backwash_time` int(11) NOT NULL,
  `backwash_countdown` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backwash`
--

INSERT INTO `backwash` (`backwash_id`, `backwash_state_1`, `backwash_start_1`, `backwash_end_1`, `backwash_status_1`, `backwash_state_2`, `backwash_start_2`, `backwash_end_2`, `backwash_status_2`, `backwash_state_3`, `backwash_start_3`, `backwash_end_3`, `backwash_status_3`, `backwash_state_4`, `backwash_start_4`, `backwash_end_4`, `backwash_status_4`, `backwash_mode`, `backwash_time`, `backwash_countdown`) VALUES
(1, '[\"1\",\"2\",\"3\",\"4\"]', '10:27:00', '10:27:20', 1, '[\"4\"]', '22:00:00', '22:01:00', 1, '', '00:00:00', '00:00:00', 0, '', '00:00:00', '00:00:00', 0, 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `chlorine`
--

CREATE TABLE `chlorine` (
  `chlorine_id` int(1) NOT NULL,
  `chlorine_1` int(1) NOT NULL,
  `chlorine_2` int(1) NOT NULL,
  `chlorine_3` int(1) NOT NULL,
  `chlorine_4` int(1) NOT NULL,
  `chlorine_5` int(1) NOT NULL,
  `chlorine_6` int(1) NOT NULL,
  `chlorine_7` int(1) NOT NULL,
  `chlorine_8` int(1) NOT NULL,
  `chlorine_9` int(1) NOT NULL,
  `chlorine_10` int(1) NOT NULL,
  `chlorine_11` int(1) NOT NULL,
  `chlorine_12` int(1) NOT NULL,
  `chlorine_13` int(1) NOT NULL,
  `chlorine_14` int(1) NOT NULL,
  `chlorine_15` int(1) NOT NULL,
  `chlorine_16` int(1) NOT NULL,
  `chlorine_17` int(1) NOT NULL,
  `chlorine_18` int(1) NOT NULL,
  `chlorine_19` int(1) NOT NULL,
  `chlorine_20` int(1) NOT NULL,
  `chlorine_21` int(1) NOT NULL,
  `chlorine_22` int(1) NOT NULL,
  `chlorine_23` int(1) NOT NULL,
  `chlorine_24` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `chlorine`
--

INSERT INTO `chlorine` (`chlorine_id`, `chlorine_1`, `chlorine_2`, `chlorine_3`, `chlorine_4`, `chlorine_5`, `chlorine_6`, `chlorine_7`, `chlorine_8`, `chlorine_9`, `chlorine_10`, `chlorine_11`, `chlorine_12`, `chlorine_13`, `chlorine_14`, `chlorine_15`, `chlorine_16`, `chlorine_17`, `chlorine_18`, `chlorine_19`, `chlorine_20`, `chlorine_21`, `chlorine_22`, `chlorine_23`, `chlorine_24`) VALUES
(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `dashboard`
--

CREATE TABLE `dashboard` (
  `dashboard_id` int(1) NOT NULL,
  `pressure` double NOT NULL,
  `ph` double NOT NULL,
  `orp` double NOT NULL,
  `temperature` double NOT NULL,
  `filtration` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pompe_ozone` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `chauffage` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `chauffage2` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lamp_zone1` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lamp_zone2` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lamp_uv` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `volt_1` double NOT NULL,
  `volt_2` double NOT NULL,
  `volt_3` double NOT NULL,
  `max_pressure` double DEFAULT NULL,
  `min_pressure` double DEFAULT NULL,
  `plc_in` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `plc_out` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `relay_ch` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `count_down` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `dashboard`
--

INSERT INTO `dashboard` (`dashboard_id`, `pressure`, `ph`, `orp`, `temperature`, `filtration`, `pompe_ozone`, `chauffage`, `chauffage2`, `lamp_zone1`, `lamp_zone2`, `lamp_uv`, `volt_1`, `volt_2`, `volt_3`, `max_pressure`, `min_pressure`, `plc_in`, `plc_out`, `relay_ch`, `count_down`) VALUES
(1, 0, 0, 0, 0, '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '0', '0', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `data_logger`
--

CREATE TABLE `data_logger` (
  `data_logger_id` bigint(20) NOT NULL,
  `machine_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_time` datetime NOT NULL,
  `ph` double NOT NULL,
  `orp` double NOT NULL,
  `temp` double NOT NULL,
  `pressure` double NOT NULL,
  `plc_q0` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `plc_q1` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `plc_q2` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `plc_q3` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `plc_i0` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `plc_i1` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `plc_i2` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `plc_i3` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `plc_i4` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `plc_i5` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `plc_i6` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `plc_i7` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `relay_ch0` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `relay_ch1` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `relay_ch2` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `relay_ch3` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `relay_ch4` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `relay_ch5` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `relay_ch6` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `relay_ch7` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_create` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_logger`
--

INSERT INTO `data_logger` (`data_logger_id`, `machine_code`, `date_time`, `ph`, `orp`, `temp`, `pressure`, `plc_q0`, `plc_q1`, `plc_q2`, `plc_q3`, `plc_i0`, `plc_i1`, `plc_i2`, `plc_i3`, `plc_i4`, `plc_i5`, `plc_i6`, `plc_i7`, `relay_ch0`, `relay_ch1`, `relay_ch2`, `relay_ch3`, `relay_ch4`, `relay_ch5`, `relay_ch6`, `relay_ch7`, `date_create`) VALUES
(1, 'MA', '2024-01-04 09:22:10', 1, 1, 1, 1, '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '2024-01-04'),
(2, 'MA', '2024-01-04 09:22:10', 1, 1, 1, 1, '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '2024-01-04');

-- --------------------------------------------------------

--
-- Table structure for table `filtration_time`
--

CREATE TABLE `filtration_time` (
  `ft_id` int(1) NOT NULL,
  `ft_status_1` int(1) NOT NULL COMMENT '0.close 1.swim 2.ozone choc',
  `ft_zone_1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ft_status_2` int(1) NOT NULL,
  `ft_zone_2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ft_status_3` int(1) NOT NULL,
  `ft_zone_3` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ft_status_4` int(11) NOT NULL,
  `ft_zone_4` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ft_status_5` int(1) NOT NULL,
  `ft_zone_5` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ft_status_6` int(1) NOT NULL,
  `ft_zone_6` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ft_status_7` int(1) NOT NULL,
  `ft_zone_7` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ft_status_8` int(1) NOT NULL,
  `ft_zone_8` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ft_status_9` int(1) NOT NULL,
  `ft_zone_9` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ft_status_10` int(1) NOT NULL,
  `ft_zone_10` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ft_status_11` int(1) NOT NULL,
  `ft_zone_11` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ft_status_12` int(1) NOT NULL,
  `ft_zone_12` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ft_status_13` int(1) NOT NULL,
  `ft_zone_13` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ft_status_14` int(1) NOT NULL,
  `ft_zone_14` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ft_status_15` int(1) NOT NULL,
  `ft_zone_15` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ft_status_16` int(1) NOT NULL,
  `ft_zone_16` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ft_status_17` int(1) NOT NULL,
  `ft_zone_17` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ft_status_18` int(1) NOT NULL,
  `ft_zone_18` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ft_status_19` int(1) NOT NULL,
  `ft_zone_19` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ft_status_20` int(1) NOT NULL,
  `ft_zone_20` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ft_status_21` int(1) NOT NULL,
  `ft_zone_21` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ft_status_22` int(1) NOT NULL,
  `ft_zone_22` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ft_status_23` int(1) NOT NULL,
  `ft_zone_23` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ft_status_24` int(1) NOT NULL,
  `ft_zone_24` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `filtration_time`
--

INSERT INTO `filtration_time` (`ft_id`, `ft_status_1`, `ft_zone_1`, `ft_status_2`, `ft_zone_2`, `ft_status_3`, `ft_zone_3`, `ft_status_4`, `ft_zone_4`, `ft_status_5`, `ft_zone_5`, `ft_status_6`, `ft_zone_6`, `ft_status_7`, `ft_zone_7`, `ft_status_8`, `ft_zone_8`, `ft_status_9`, `ft_zone_9`, `ft_status_10`, `ft_zone_10`, `ft_status_11`, `ft_zone_11`, `ft_status_12`, `ft_zone_12`, `ft_status_13`, `ft_zone_13`, `ft_status_14`, `ft_zone_14`, `ft_status_15`, `ft_zone_15`, `ft_status_16`, `ft_zone_16`, `ft_status_17`, `ft_zone_17`, `ft_status_18`, `ft_zone_18`, `ft_status_19`, `ft_zone_19`, `ft_status_20`, `ft_zone_20`, `ft_status_21`, `ft_zone_21`, `ft_status_22`, `ft_zone_22`, `ft_status_23`, `ft_zone_23`, `ft_status_24`, `ft_zone_24`) VALUES
(1, 0, '00:00 - 00:59', 0, '01:00 - 01:59', 0, '02:00 - 02:59', 0, '03:00 - 03:59', 0, '04:00 - 04:59', 0, '05:00 - 05:59', 0, '06:00 - 06:59', 0, '07:00 - 07:59', 0, '08:00 - 08:59', 1, '09:00 - 09:59', 1, '10:00 - 10:59', 1, '11:00 - 11:59', 1, '12:00 - 12:59', 2, '13:00 - 13:59', 2, '14:00 - 14:59', 2, '15:00 - 15:59', 2, '16:00 - 16:59', 2, '17:00 - 17:59', 2, '18:00 - 18:59', 1, '19:00 - 19:59', 1, '20:00 - 20:59', 1, '21:00 - 21:59', 1, '22:00 - 22:59', 1, '23:00 - 23:59');

-- --------------------------------------------------------

--
-- Table structure for table `heatpump`
--

CREATE TABLE `heatpump` (
  `heatpump_id` int(1) NOT NULL,
  `heatpump_start` time NOT NULL,
  `heatpump_end` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `heatpump`
--

INSERT INTO `heatpump` (`heatpump_id`, `heatpump_start`, `heatpump_end`) VALUES
(1, '08:00:00', '22:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `machine`
--

CREATE TABLE `machine` (
  `machine_id` int(1) NOT NULL,
  `machine_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `machine_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `machine`
--

INSERT INTO `machine` (`machine_id`, `machine_name`, `machine_code`) VALUES
(1, 'performance_pro', 'MA-202306-002');

-- --------------------------------------------------------

--
-- Table structure for table `machine_option`
--

CREATE TABLE `machine_option` (
  `mo_id` int(1) NOT NULL,
  `volt_1_ph` int(1) NOT NULL COMMENT '0.close 1.open',
  `volt_2_ph` int(1) NOT NULL,
  `backwash` int(1) NOT NULL,
  `ph` int(1) NOT NULL,
  `orp` int(1) NOT NULL,
  `apf` int(1) NOT NULL,
  `chlorine` int(1) NOT NULL,
  `heater_1` int(1) NOT NULL,
  `heater_2` int(1) NOT NULL,
  `night_time` int(1) NOT NULL,
  `heat_pump_heater` int(1) NOT NULL,
  `heat_pump_cooling` int(1) NOT NULL,
  `heat_pump_all` int(1) NOT NULL,
  `ozone_choc` int(1) NOT NULL,
  `ozone_1` int(1) NOT NULL,
  `ozone_2` int(1) NOT NULL,
  `uv` int(1) NOT NULL,
  `air_pump` int(1) NOT NULL,
  `isaver_mode_1` int(1) NOT NULL,
  `isaver_mode_2` int(1) NOT NULL,
  `isaver_mode_3` int(1) NOT NULL,
  `isaver_mode_4` int(1) NOT NULL,
  `temperature` int(1) NOT NULL,
  `cocoon` int(1) NOT NULL,
  `vmc` int(1) NOT NULL,
  `aco` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `machine_option`
--

INSERT INTO `machine_option` (`mo_id`, `volt_1_ph`, `volt_2_ph`, `backwash`, `ph`, `orp`, `apf`, `chlorine`, `heater_1`, `heater_2`, `night_time`, `heat_pump_heater`, `heat_pump_cooling`, `heat_pump_all`, `ozone_choc`, `ozone_1`, `ozone_2`, `uv`, `air_pump`, `isaver_mode_1`, `isaver_mode_2`, `isaver_mode_3`, `isaver_mode_4`, `temperature`, `cocoon`, `vmc`, `aco`) VALUES
(1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `night_time`
--

CREATE TABLE `night_time` (
  `night_time_id` int(1) NOT NULL,
  `night_time_enable` int(1) NOT NULL COMMENT 'เปลี่ยนมาใช้จาก เว็บ 0.ปิด 1.เปิด',
  `night_time_status` int(11) NOT NULL COMMENT '0.ปิด 1.เปิด',
  `night_time_start` time NOT NULL,
  `night_time_end` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `night_time`
--

INSERT INTO `night_time` (`night_time_id`, `night_time_enable`, `night_time_status`, `night_time_start`, `night_time_end`) VALUES
(1, 0, 0, '00:00:00', '00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `orp`
--

CREATE TABLE `orp` (
  `orp_id` int(1) NOT NULL,
  `orp_1` int(1) NOT NULL,
  `orp_2` int(1) NOT NULL,
  `orp_3` int(1) NOT NULL,
  `orp_4` int(1) NOT NULL,
  `orp_5` int(1) NOT NULL,
  `orp_6` int(1) NOT NULL,
  `orp_7` int(1) NOT NULL,
  `orp_8` int(1) NOT NULL,
  `orp_9` int(1) NOT NULL,
  `orp_10` int(1) NOT NULL,
  `orp_11` int(1) NOT NULL,
  `orp_12` int(1) NOT NULL,
  `orp_13` int(1) NOT NULL,
  `orp_14` int(1) NOT NULL,
  `orp_15` int(1) NOT NULL,
  `orp_16` int(1) NOT NULL,
  `orp_17` int(1) NOT NULL,
  `orp_18` int(1) NOT NULL,
  `orp_19` int(1) NOT NULL,
  `orp_20` int(1) NOT NULL,
  `orp_21` int(1) NOT NULL,
  `orp_22` int(1) NOT NULL,
  `orp_23` int(1) NOT NULL,
  `orp_24` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orp`
--

INSERT INTO `orp` (`orp_id`, `orp_1`, `orp_2`, `orp_3`, `orp_4`, `orp_5`, `orp_6`, `orp_7`, `orp_8`, `orp_9`, `orp_10`, `orp_11`, `orp_12`, `orp_13`, `orp_14`, `orp_15`, `orp_16`, `orp_17`, `orp_18`, `orp_19`, `orp_20`, `orp_21`, `orp_22`, `orp_23`, `orp_24`) VALUES
(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ph`
--

CREATE TABLE `ph` (
  `ph_id` int(1) NOT NULL,
  `ph_1` int(1) NOT NULL,
  `ph_2` int(1) NOT NULL,
  `ph_3` int(1) NOT NULL,
  `ph_4` int(1) NOT NULL,
  `ph_5` int(1) NOT NULL,
  `ph_6` int(1) NOT NULL,
  `ph_7` int(1) NOT NULL,
  `ph_8` int(1) NOT NULL,
  `ph_9` int(1) NOT NULL,
  `ph_10` int(1) NOT NULL,
  `ph_11` int(1) NOT NULL,
  `ph_12` int(1) NOT NULL,
  `ph_13` int(1) NOT NULL,
  `ph_14` int(1) NOT NULL,
  `ph_15` int(1) NOT NULL,
  `ph_16` int(1) NOT NULL,
  `ph_17` int(1) NOT NULL,
  `ph_18` int(1) NOT NULL,
  `ph_19` int(1) NOT NULL,
  `ph_20` int(1) NOT NULL,
  `ph_21` int(1) NOT NULL,
  `ph_22` int(1) NOT NULL,
  `ph_23` int(1) NOT NULL,
  `ph_24` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ph`
--

INSERT INTO `ph` (`ph_id`, `ph_1`, `ph_2`, `ph_3`, `ph_4`, `ph_5`, `ph_6`, `ph_7`, `ph_8`, `ph_9`, `ph_10`, `ph_11`, `ph_12`, `ph_13`, `ph_14`, `ph_15`, `ph_16`, `ph_17`, `ph_18`, `ph_19`, `ph_20`, `ph_21`, `ph_22`, `ph_23`, `ph_24`) VALUES
(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `setting_id` int(1) NOT NULL,
  `setting_temperature` double NOT NULL,
  `setting_aeau` int(11) NOT NULL,
  `setting_eau` int(11) NOT NULL,
  `setting_systeme` int(11) NOT NULL,
  `setting_temp_deff` int(11) NOT NULL,
  `setting_basse` double NOT NULL,
  `setting_haute` double NOT NULL,
  `setting_tentative` double NOT NULL,
  `setting_frequence` double NOT NULL,
  `online_status` int(1) NOT NULL COMMENT '0.close 1.open',
  `temp_type` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `temp_calibrate` double NOT NULL,
  `pressure_type` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pressure_calibrate` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`setting_id`, `setting_temperature`, `setting_aeau`, `setting_eau`, `setting_systeme`, `setting_temp_deff`, `setting_basse`, `setting_haute`, `setting_tentative`, `setting_frequence`, `online_status`, `temp_type`, `temp_calibrate`, `pressure_type`, `pressure_calibrate`) VALUES
(1, 35, 3, 6, 5, 2, 0.2, 1, 15, 3, 0, 'minus', 0, 'plus', 0);

-- --------------------------------------------------------

--
-- Table structure for table `setting_mode`
--

CREATE TABLE `setting_mode` (
  `sm_id` int(1) NOT NULL,
  `sm_filtration` int(1) NOT NULL COMMENT '0.close 1.open 2.auto',
  `sm_bypass` int(1) NOT NULL COMMENT '0.close 1.open',
  `sm_ozone_choc` int(1) NOT NULL COMMENT '0.close 1.open 2.auto',
  `sm_chauffage` int(1) NOT NULL COMMENT '0.close 1.open',
  `sm_pomp_ozone` int(1) NOT NULL COMMENT '0.close 1.open 2.auto',
  `sm_lamp_ozone` int(1) NOT NULL COMMENT '0.close 1.open 2.auto',
  `sm_lamp_uv` int(1) NOT NULL COMMENT '0.close 1.open 2.auto',
  `sm_pump_air` int(1) NOT NULL COMMENT '0.close 1.open 2.auto'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `setting_mode`
--

INSERT INTO `setting_mode` (`sm_id`, `sm_filtration`, `sm_bypass`, `sm_ozone_choc`, `sm_chauffage`, `sm_pomp_ozone`, `sm_lamp_ozone`, `sm_lamp_uv`, `sm_pump_air`) VALUES
(1, 1, 1, 2, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `substance`
--

CREATE TABLE `substance` (
  `substance_id` int(1) NOT NULL,
  `ph_set` double NOT NULL,
  `ph_lower` double NOT NULL,
  `ph_inj` double NOT NULL,
  `ph_freq` double NOT NULL,
  `orp_set` int(11) NOT NULL,
  `orp_lower` int(11) NOT NULL,
  `orp_inj` double NOT NULL,
  `orp_freq` double NOT NULL,
  `apf_set` double NOT NULL,
  `apf_lower` double NOT NULL,
  `apf_inj` double NOT NULL,
  `apf_freq` double NOT NULL,
  `chlorine_inj` double NOT NULL,
  `chlorine_freq` double NOT NULL,
  `aco_inj` double NOT NULL,
  `aco_freq` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `substance`
--

INSERT INTO `substance` (`substance_id`, `ph_set`, `ph_lower`, `ph_inj`, `ph_freq`, `orp_set`, `orp_lower`, `orp_inj`, `orp_freq`, `apf_set`, `apf_lower`, `apf_inj`, `apf_freq`, `chlorine_inj`, `chlorine_freq`, `aco_inj`, `aco_freq`) VALUES
(1, 7.3, 7.25, 1, 10, 600, 650, 3, 10, 7.3, 7.25, 1, 2, 1, 3, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `vmc`
--

CREATE TABLE `vmc` (
  `vmc_id` int(1) NOT NULL,
  `vmc_filtration` int(11) NOT NULL,
  `vmc_choc` int(11) NOT NULL,
  `vmc_backwash` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vmc`
--

INSERT INTO `vmc` (`vmc_id`, `vmc_filtration`, `vmc_choc`, `vmc_backwash`) VALUES
(1, 50, 70, 100);

-- --------------------------------------------------------

--
-- Table structure for table `volt_logger`
--

CREATE TABLE `volt_logger` (
  `volt_logger` bigint(20) NOT NULL,
  `machine_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `va` double NOT NULL,
  `vb` double NOT NULL,
  `vc` double NOT NULL,
  `date_create` date DEFAULT NULL,
  `date_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aco`
--
ALTER TABLE `aco`
  ADD PRIMARY KEY (`aco_id`);

--
-- Indexes for table `administrator`
--
ALTER TABLE `administrator`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `apf`
--
ALTER TABLE `apf`
  ADD PRIMARY KEY (`apf_id`);

--
-- Indexes for table `backwash`
--
ALTER TABLE `backwash`
  ADD PRIMARY KEY (`backwash_id`);

--
-- Indexes for table `chlorine`
--
ALTER TABLE `chlorine`
  ADD PRIMARY KEY (`chlorine_id`);

--
-- Indexes for table `dashboard`
--
ALTER TABLE `dashboard`
  ADD PRIMARY KEY (`dashboard_id`);

--
-- Indexes for table `data_logger`
--
ALTER TABLE `data_logger`
  ADD PRIMARY KEY (`data_logger_id`);

--
-- Indexes for table `filtration_time`
--
ALTER TABLE `filtration_time`
  ADD PRIMARY KEY (`ft_id`);

--
-- Indexes for table `heatpump`
--
ALTER TABLE `heatpump`
  ADD PRIMARY KEY (`heatpump_id`);

--
-- Indexes for table `machine`
--
ALTER TABLE `machine`
  ADD PRIMARY KEY (`machine_id`);

--
-- Indexes for table `machine_option`
--
ALTER TABLE `machine_option`
  ADD PRIMARY KEY (`mo_id`);

--
-- Indexes for table `night_time`
--
ALTER TABLE `night_time`
  ADD PRIMARY KEY (`night_time_id`);

--
-- Indexes for table `orp`
--
ALTER TABLE `orp`
  ADD PRIMARY KEY (`orp_id`);

--
-- Indexes for table `ph`
--
ALTER TABLE `ph`
  ADD PRIMARY KEY (`ph_id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `setting_mode`
--
ALTER TABLE `setting_mode`
  ADD PRIMARY KEY (`sm_id`);

--
-- Indexes for table `substance`
--
ALTER TABLE `substance`
  ADD PRIMARY KEY (`substance_id`);

--
-- Indexes for table `vmc`
--
ALTER TABLE `vmc`
  ADD PRIMARY KEY (`vmc_id`);

--
-- Indexes for table `volt_logger`
--
ALTER TABLE `volt_logger`
  ADD PRIMARY KEY (`volt_logger`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aco`
--
ALTER TABLE `aco`
  MODIFY `aco_id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `administrator`
--
ALTER TABLE `administrator`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `apf`
--
ALTER TABLE `apf`
  MODIFY `apf_id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `backwash`
--
ALTER TABLE `backwash`
  MODIFY `backwash_id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `chlorine`
--
ALTER TABLE `chlorine`
  MODIFY `chlorine_id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `dashboard`
--
ALTER TABLE `dashboard`
  MODIFY `dashboard_id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `data_logger`
--
ALTER TABLE `data_logger`
  MODIFY `data_logger_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `filtration_time`
--
ALTER TABLE `filtration_time`
  MODIFY `ft_id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `heatpump`
--
ALTER TABLE `heatpump`
  MODIFY `heatpump_id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `machine`
--
ALTER TABLE `machine`
  MODIFY `machine_id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `machine_option`
--
ALTER TABLE `machine_option`
  MODIFY `mo_id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `night_time`
--
ALTER TABLE `night_time`
  MODIFY `night_time_id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `orp`
--
ALTER TABLE `orp`
  MODIFY `orp_id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ph`
--
ALTER TABLE `ph`
  MODIFY `ph_id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `setting_id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `setting_mode`
--
ALTER TABLE `setting_mode`
  MODIFY `sm_id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `substance`
--
ALTER TABLE `substance`
  MODIFY `substance_id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vmc`
--
ALTER TABLE `vmc`
  MODIFY `vmc_id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `volt_logger`
--
ALTER TABLE `volt_logger`
  MODIFY `volt_logger` bigint(20) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
